<?php
    $_helper = $this->helper('catalog/output');
    $_product = $this->getProduct();
?>

<?php
$_storeId = Mage::app()->getStore()->getId();
$_isActive = Mage::getStoreConfig('outofstocksubscription/mail/active', $_storeId);
if(!$_product->isSaleable() && $_isActive):
    $_url = $this->getUrl('outofstocksubscription');
else:
    $_url = $this->getSubmitUrl($_product);
endif;
?>

<script type="text/javascript">
    var optionsPrice = new Product.OptionsPrice(<?php echo $this->getJsonConfig() ?>);
</script>
<div id="messages_product_view"><?php echo $this->getMessagesBlock()->getGroupedHtml() ?></div>
<div class="product-view">
    <div class="product-essential">
        <form action="<?php echo $_url; ?>" method="post" id="product_addtocart_form"<?php if($_product->getOptions()): ?> enctype="multipart/form-data"<?php endif; ?>>
            <?php echo $this->getBlockHtml('formkey') ?>
            <div class="no-display">
                <input type="hidden" name="product" value="<?php echo $_product->getId() ?>" />
                <input type="hidden" name="related_product" id="related-products-field" value="" />
            </div>
            <div class="container-product-information">

                <div class="left-column">
                    <div class="product-img-box">
                        <div class="label-container">
                            <?php
                            //executa o product labels se o mesmo estiver ativo na loja
                            if(Mage::getConfig()->getModuleConfig('EM_Productlabels')->is('active', 'true')) {
                                Mage::helper('productlabels')->display($_product,'image');
                            }
                            ?>
                        </div>
                        <?php echo $this->getChildHtml('media') ?>
                    </div>
                    <ul class="sharing-btns">
                        <li class="sharing-btn twitter-btn">
                            <a href="<?php echo $this->helper('core/url')->getCurrentUrl(); ?>" class="twitter-share-button"  data-lang="pt_BR" data-size="small" data-count="true">Tweet</a>
                        </li>
                        <li class="sharing-btn gplus-btn">
                            <div class="g-plusone" data-size="medium"></div>
                        </li>
                        <li class="sharing-btn fb-btn">
                            <div id="fb-root"></div><div class="fb-like" data-href="<?php echo $this->helper('core/url')->getCurrentUrl(); ?>" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
                        </li>
                        <li class="sharing-btn email-btn">
                            <a class="send-mail-prod" title="<?php echo $this->__('Email to a Friend') ?>" href="<?php echo $this->helper('catalog/product')->getEmailToFriendUrl($_product) ?>"><?php echo $this->__('Email') ?></a>
                        </li>
                    </ul>
                </div>

                <div class="product-shop">

                    <div class="product-name">
                        <h1><?php echo $_helper->productAttribute($_product, $_product->getName(), 'name') ?></h1>
                        <div class="extra-info info-top">
                            <?php echo $this->getReviewsSummaryHtml($_product, 'default', false)?>
                        </div>
                    </div>

                    <?php if ($_product->getShortDescription()):?>
                        <div class="short-description">
                            <div class="std"><?php echo $_helper->productAttribute($_product, ($_product->getShortDescription()), 'short_description') ?></div>
                        </div>
                    <?php endif;?>

                    <?php echo $this->getChildHtml('alert_urls') ?>

                    <?php echo $this->getChildHtml('other');?>

                    <?php if ($_product->isSaleable() && $this->hasOptions()):?>
                        <?php echo $this->getChildChildHtml('container1', '', true, true) ?>
                    <?php endif;?>

                    <div class="add-to-cart-wrapper">

                        <?php echo $this->getChildHtml('product_type_availability'); ?>
                        <?php if(!$_product->isSaleable() && $_isActive): ?>
                            <?php echo $this->getChildHtml('out_of_stock') ?>
                        <?php else: ?>

                            <?php echo $this->getChildHtml('product_type_data') ?>

                            <?php if (!$this->hasOptions()):?>
                                <div class="add-to-box">
                                    <?php if($_product->isSaleable()): ?>
                                        <?php echo $this->getChildHtml('addtocart') ?>
                                        <?php if( $this->helper('wishlist')->isAllow() || $_compareUrl=$this->helper('catalog/product_compare')->getAddUrl($_product)): ?>
                                            <span class="or"><?php echo $this->__('OR') ?></span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <?php echo $this->getChildHtml('extra_buttons') ?>
                            <?php endif; ?>

                            <?php if ($_product->isSaleable() && $this->hasOptions()):?>
                                <?php echo $this->getChildChildHtml('container2', '', true, true) ?>
                                <a href="#review-form" class="review-link"><?php echo $this->__('Avaliar') ?></a>
                            <?php endif;?>

                            <?php if($_product->isSaleable() && $_isActive): ?>
                                <div class="price-info">
                                    <?php echo $this->getPriceHtml($_product); ?>
                                    <?php echo $this->getChildHtml('bundle_prices') ?>
                                    <?php echo $this->getChildHtml('boleto_parcelas') ?>
                                    <?php echo $this->getTierPriceHtml() ?>
                                    <?php echo $this->getChildHtml('extrahint') ?>
                                </div>
                            <?php endif;?>

                            <?php if (!$this->hasOptions()):?>
                                <div class="add-to-box">
                                    <?php if($_product->isSaleable()): ?>
                                        <?php if( $this->helper('wishlist')->isAllow() || $_compareUrl=$this->helper('catalog/product_compare')->getAddUrl($_product)): ?>
                                            <span class="or"><?php echo $this->__('OR') ?></span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php echo $this->getChildHtml('addto') ?>
                                    <a href="#review-form" class="review-link"><?php echo $this->__('Avaliar') ?></a>
                                </div>
                                <?php echo $this->getChildHtml('extra_buttons') ?>
                            <?php elseif (!$_product->isSaleable()): ?>
                                <div class="add-to-box">
                                    <?php echo $this->getChildHtml('addto') ?>
                                    <a href="#review-form" class="review-link"><?php echo $this->__('Avaliar') ?></a>
                                </div>
                            <?php endif; ?>

                            <?php if($_product->isSaleable()): ?>
                                <?php echo $this->getChildHtml('frete') ?>
                            <?php endif; ?>


                        <?php endif; ?>
                    </div>
                    <?php if ($_product->isSaleable() && $this->hasOptions()):?>
                        <?php echo $this->getChildChildHtml('container1', '', true, true) ?>
                        <?php $_wishlistSubmitUrl = $this->helper('wishlist')->getAddUrl($_product); ?>
                        <ul class="add-to-links">
                            <?php if ($this->helper('wishlist')->isAllow()) : ?>
                                <li><a href="<?php echo $this->helper('wishlist')->getAddUrl($_product) ?>" class="link-wishlist"><?php echo $this->__('Favoritos') ?></a></li>
                            <?php endif; ?>
                        </ul>
                        <a href="#review-form" class="review-link"><?php echo $this->__('Avaliar') ?></a>
                    <?php endif;?>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            //<![CDATA[
            var productAddToCartForm = new VarienForm('product_addtocart_form');
            productAddToCartForm.submit = function(button, url) {
                if (this.validator.validate()) {
                    var form = this.form;
                    var oldUrl = form.action;

                    if (url) {
                        form.action = url;
                    }
                    var e = null;
                    try {
                        this.form.submit();
                    } catch (e) {
                    }
                    this.form.action = oldUrl;
                    if (e) {
                        throw e;
                    }

                    if (button && button != 'undefined') {
                        button.disabled = true;
                    }
                }
            }.bind(productAddToCartForm);

            productAddToCartForm.submitLight = function(button, url){
                if(this.validator) {
                    var nv = Validation.methods;
                    delete Validation.methods['required-entry'];
                    delete Validation.methods['validate-one-required'];
                    delete Validation.methods['validate-one-required-by-name'];
                    // Remove custom datetime validators
                    for (var methodName in Validation.methods) {
                        if (methodName.match(/^validate-datetime-.*/i)) {
                            delete Validation.methods[methodName];
                        }
                    }

                    if (this.validator.validate()) {
                        if (url) {
                            this.form.action = url;
                        }
                        this.form.submit();
                    }
                    Object.extend(Validation.methods, nv);
                }
            }.bind(productAddToCartForm);
            //]]>
        </script>
    </div>

    <div class="product-collateral toggle-content tabs">
        <?php if ($detailedInfoGroup = $this->getChildGroup('detailed_info', 'getChildHtml')):?>
            <dl id="collateral-tabs" class="collateral-tabs">
                <?php foreach ($detailedInfoGroup as $alias => $html):?>
                    <dt class="tab"><span><?php echo $this->escapeHtml($this->getChildData($alias, 'title')) ?></span></dt>
                    <dd class="tab-container">
                        <div class="tab-content"><?php echo $html ?></div>
                    </dd>
                <?php endforeach;?>
            </dl>
        <?php endif; ?>
    </div>
    <?php echo $this->getChildHtml('related_products') ?>

    <?php echo $this->getChildHtml('upsell_products') ?>
    <?php echo $this->getChildHtml('product_additional_data') ?>

</div>
<script type="text/javascript">
    window.___gcfg = {lang: 'pt-BR'};
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    window.fbAsyncInit = function() {
        FB.init({appId: '', status: true, cookie: true, xfbml: true});};
    (function() {
        var e = document.createElement('script');
        e.type = 'text/javascript';
        e.src = document.location.protocol + '//connect.facebook.net/pt_BR/all.js#xfbml=1';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    jQuery("a[href='#review-form']").click(function(){
        jQuery(".product-collateral.toggle-content.tabs li:last-child").click();
    })
</script>