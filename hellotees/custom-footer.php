<div class="footer-bar bar-1">
    <div class="footer-line full-line">
        <?php echo $this->getChildHtml("newsletter"); ?>
    </div>
    <div class="footer-line line1">
        <div class="left-container">
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_contacts')->toHtml(); ?>
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('custom_footer')->toHtml(); ?>
        </div>
        <div class="right-container">
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('fb-like-box')->toHtml(); ?>
        </div>
    </div>
</div>
<div class="footer-bar bar-2">
    <div class="footer-line line3">
        <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_icons')->toHtml(); ?>
    </div>
</div>