<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-language-container f-left">
            <i class="fa fa-phone"></i> Telefones: <?php echo $this->getTelefone(); ?>
            Segunda � Sexta : 9h �s 18:45h
            S�bado: 9h �s 17h
        </div>
        <div class="f-right">
            <!-- Mensagem de boas-vindas -->
            <?php echo $this->getWelcome() ?>

            <a href="<?php echo $this->getUrl('lista-de-casamento-supreme-inox-tramontina');?>" class="btn-wedding" title="Lista de Casamento">
                <img src="/media/images/default/lista-de-casamento-topo.jpg" alt="Lista de Casamento" />
            </a>
            <!-- Link para logout -->
            <?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>
                <span class="logout-link f-right">
				[<a href="<?php echo $this->getUrl('customer/account/logout/'); ?>" title="Encerrar sess�o">sair</a>]
			</span>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="header-container">
    <header id="header" class="page-header">
        <div class="page-header-container">
            <a class="logo" href="<?php echo $this->getUrl('') ?>">
                <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
                <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
            </a>

            <!-- Skip Links -->
            <div class="skip-links">
                <a href="#header-nav" class="skip-link skip-nav">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Menu'); ?></span>
                </a>

                <a href="#header-search" class="skip-link skip-search">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Search'); ?></span>
                </a>

                <a href="#header-account" class="skip-link skip-account">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Account'); ?></span>
                </a>

                <!-- Cart -->
                <div class="header-minicart">
                    <?php echo $this->getChildHtml('minicart_head'); ?>
                    <span class="mini-cart-subtotal">
                        <?php
                        $cartSubtotal = Mage::getSingleton('checkout/session')->getQuote()->getSubtotal();
                        echo Mage::helper('checkout')->formatPrice($cartSubtotal);
                        ?>
                    </span>
                    <span class="mini-cart-itens">
                        <?php
                        $count = $this->helper('checkout/cart')->getSummaryCount();  //get total items in cart
                        if($count==0) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(0 itens)</a>',$count);
                        }
                        if($count==1) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(1 item)</a>',$count);
                        }
                        if($count>1) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(%s itens)</a>',$count);
                        }
                        ?>
                    </span>
                </div>
            </div>

            <!-- Search -->
            <div id="header-search" class="skip-content">
                <?php echo $this->getChildHtml('topSearch') ?>
            </div>

            <div class="atendimento-topo">
                <script type="text/javascript" language="JavaScript" src="https://www.chatcomercial.com.br/livehelp/lib/javascript/support_status.php?COMPANY_ID=18657&amp;SITE_ID=22030&amp;ssl=1"></script>
            </div>

            <ul class="links">
                <li class="top-wishlist"><a href="<?php echo $this->getUrl('wishlist'); ?>" title="Meus Favoritos" class="wishlist-link">Meus Favoritos</a></li>
                <li class="top-orders"><a href="<?php echo $this->getUrl('sales/order/history/'); ?>" title="Meus Pedidos" class="top-link-account">Meus Pedidos</a></li>
            </ul>

            <!-- Account -->
            <div id="header-account" class="skip-content">
                <?php echo $this->getChildHtml('topLinks') ?>
            </div>
        </div>
        <!-- Navigation -->
        <div id="header-nav" class="skip-content">
            <?php echo $this->getChildHtml('topMenu') ?>
        </div>
        <?php echo $this->getAdditionalHtml() ?>

        <div class="top-block-wrapper">
            <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('top_block')->toHtml() ?>
        </div>

        <div class="top-block-wrapper">
            <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('top_block_2')->toHtml() ?>
        </div>

    </header>
</div>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>

<!-- Selos de benef�cios -->
<div class="parent-container">
    <?php echo $this->getBeneficios(); ?>
</div>