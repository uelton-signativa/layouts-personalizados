Validation.add('validate-cpfcnpj','O número digitado é inválido.',function(v){
    var regexPF = /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/;
    var regexPJ = /^[0-9]{2}\.[0-9]{3}\.[0-9]{3}\/[0-9]{4}\-[0-9]{2}$/;

    if (regexPF.test(v) || v.length == 11) {

        var cpf = v;
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" );

        if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
            return false;

        var add = 0;
        for (i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);

        var rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;

        if (rev != parseInt(cpf.charAt(9)))
            return false;

        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);

        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;

        if (rev != parseInt(cpf.charAt(10)))
            return false;

        return true;

    } else

    if (regexPJ.test(v)) {

        var cnpj = v;
        exp = /\.|\-|\//g
        cnpj = cnpj.toString().replace( exp, "" );

        if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999")
            return false;

        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0,tamanho);
        var digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }

        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;

    } else
        return false;

});

function signativaMascaraTaxVat(o) {
    if (jQuery(o).val().length > 14)
        signativaMascaraTaxVatTimeout(jQuery(o), signativaMascaraCNPJ);
    else
        signativaMascaraTaxVatTimeout(jQuery(o), signativaMascaraCPF);
}

function signativaMascaraTaxVatTimeout(o, f) {
    v_obj = jQuery(o);
    v_fun = f;
    setTimeout("signativaExecutaMascara()", 1);
}

function signativaExecutaMascara() {
    v_obj.val(v_fun(v_obj.val()));
}

function signativaMascaraCPF(v) {
    v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
    v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto e re o terceiro e o quarto dígitos
    v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
}

function signativaMascaraCNPJ(v) {
    v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
    v = v.replace( /^(\d{2})(\d)/ , "$1.$2"); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace( /^(\d{2})\.(\d{3})(\d)/ , "$1.$2.$3"); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace( /\.(\d{3})(\d)/ , ".$1/$2"); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace( /(\d{4})(\d)/ , "$1-$2"); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}

jQuery(document).ready(function(){
    jQuery('input[name="billing[taxvat]"]').attr('maxlength',18).addClass('validate-cpfcnpj').keypress(function() {
        signativaMascaraTaxVat(jQuery(this));
    });

    jQuery('input[name="taxvat"]').attr('maxlength',18).addClass('validate-cpfcnpj').keypress(function() {
        signativaMascaraTaxVat(jQuery(this));
    });
});