function estadoBR(uf) {
    var obj = {
        'AC': 'Acre',
        'AL': 'Alagoas',
        'AM': 'Amazonas',
        'AP': 'Amapá',
        'BA': 'Bahia',
        'CE': 'Ceará',
        'DF': 'Distrito Federal',
        'ES': 'Espírito Santo',
        'GO': 'Goiás',
        'MA': 'Maranhão',
        'MT': 'Mato Grosso',
        'MS': 'Mato Grosso do Sul',
        'MG': 'Minas Gerais',
        'PA': 'Pará',
        'PB': 'Paraíba',
        'PR': 'Paraná',
        'PE': 'Pernambuco',
        'PI': 'Piauí',
        'RJ': 'Rio de Janeiro',
        'RN': 'Rio Grande do Norte',
        'RO': 'Rondônia',
        'RS': 'Rio Grande do Sul',
        'RR': 'Roraima',
        'SC': 'Santa Catarina',
        'SE': 'Sergipe',
        'SP': 'São Paulo',
        'TO': 'Tocantins'
    };
    for (key in obj){
        if (key == uf){
            return obj[key];
        }
    }
}
jQuery.fn.postcodeAjax = function( settings, noFocus){
    var l = window.location;
    var base_url = l.protocol + "//" + l.host + "/";

    var configuration = {
        postcode:      	'input[name="billing[postcode]"]',
        street:        	'input[id="billing:street1"]',
        number:        	'input[id="billing:street2"]',
        district:      	'input[id="billing:street4"]',
        city:          	'input[name="billing[city]"]',
        region:        	'input[name="billing[region_id]"]',
        rhidden:       	'input[name="billing[region]"]',
        regionSel:		'select[name="billing[region_id]"]',
        select:        	false,
        linkBase:      	base_url
    };
    var codigoEstado;
    if (settings)
        jQuery.extend(configuration, settings);

    if(jQuery(configuration.postcode).val() != ""){

        jQuery('.window-overlay').fadeIn(300,function(){
            jQuery('.busca-cep').html('<img src="'+configuration.linkBase+'skin/frontend/signa_base/default/images/opc-ajax-loader.gif" alt="Carregando..." title="Carregando..." class="v-middle"> Aguarde... Procurando endereço.').show();
        });

        jQuery.ajaxSetup({ cache: true });
        jQuery.getScript("/allCepAjax.php?cep=" + jQuery(configuration.postcode).val(), function() {
            if(resultadoCEP["resultado"] != 0){
                if (resultadoCEP["tipo_logradouro"]!= ""){jQuery(configuration.street).val(unescape(resultadoCEP["tipo_logradouro"] + ' ' + resultadoCEP["logradouro"])).prop('readonly', true).addClass('disabled-input');}
                else{jQuery(configuration.street).val("").prop('readonly', false).removeClass('disabled-input');}
                if (resultadoCEP['bairro']!=""){jQuery(configuration.district).val(unescape(resultadoCEP["bairro"])).prop('readonly', true).addClass('disabled-input');}
                else{jQuery(configuration.district).val("").prop('readonly', false).removeClass('disabled-input')}
                jQuery(configuration.city).val(unescape(resultadoCEP["cidade"])).prop('readonly',true).addClass('disabled-input');
                jQuery(configuration.rhidden).val(resultadoCEP["uf"]);
                jQuery( configuration.regionSel+' option').each(function(ordem, obj){
                    if (obj.value){
                        if (jQuery(obj).attr('title') == estadoBR(resultadoCEP["uf"])){
                            codigoEstado = jQuery(obj).val();
                        }
                    }
                });

                jQuery(configuration.regionSel+' option:selected').removeAttr('selected');
                jQuery(configuration.regionSel).val(codigoEstado);
                if (resultadoCEP["logradouro"] && noFocus)
                    jQuery(configuration.number).focus();
                else if(noFocus)
                    jQuery(configuration.street).focus();
                jQuery('.window-overlay').fadeOut(300,function(){
                    jQuery('.busca-cep').fadeOut(300);
                });
            } else {
                jQuery('.busca-cep').html('CEP não encontrado.');
                setTimeout(function() {
                    jQuery('.window-overlay').fadeOut(300,function(){
                        jQuery('.busca-cep').fadeOut(300);
                    });
                }, 1000);
                jQuery(configuration.street).focus();
                jQuery(configuration.street).val('');
                jQuery(configuration.district).val('');
                jQuery(configuration.city).val('').prop('readonly',false).removeClass('disabled-input');
                jQuery(configuration.number).val('');
            }
            //jQuery(configuration.region).children('option')[e].value = '';
        });
    }
}
jQuery(document).ready(function(){
    jQuery('body').append('<span class="please-wait busca-cep"></span>');
    jQuery('.busca-cep').hide();

    jQuery.mask.definitions['~'] = "[+-]";
    /*Bloqueia campos na conta do usuário*/

    jQuery('#street_1').focus(function(){
        if (jQuery(this).val()){
            jQuery(this).prop('readonly', true).addClass('disabled-input');    
            jQuery('input[name="postcode"]').focus();
        }
    });
    jQuery('#street_4').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');
            jQuery('input[name="postcode"]').focus();
        }
    });
    jQuery('input[name="city"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="postcode"]').focus();
        }
    });
    /*Bloqueia campos na cobrança*/
    jQuery('input[id="billing:street1"]').focus(function(){
            //alert("uhe");//
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="billing[postcode]"]').focus();
        }
    });
    jQuery('input[id="billing:street4"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="billing[postcode]"]').focus();
        }
    });
    jQuery('input[name="billing[city]"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="billing[postcode]"]').focus();
        }
    });
    /*Bloqueia campos na Entrega*/
    jQuery('input[id="shipping:street1"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="shipping[postcode]"]').focus();
        }
    });
    jQuery('input[id="shipping:street4"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="shipping[postcode]"]').focus();
        }
    });
    jQuery('input[name="shipping[city]"]').focus(function(){
        if (jQuery(this).val()){            
            jQuery(this).prop('readonly', true).addClass('disabled-input');        
            jQuery('input[name="shipping[postcode]"]').focus();
        }
    });
    /* Máscaras no campo de CEP da Página do Produto */
    jQuery('input[name="estimate[postcode]"]').mask("99999-999");

    /* Máscaras no campo de CEP do Carrinho de Compras */
    jQuery('input[name="estimate_postcode"]').mask("99999-999");

    /* Máscaras nos campos de endereço de cobrança (one page checkout) */
    jQuery('input[name="billing[postcode]"]').mask("99999-999");
    jQuery('input[name="billing[telephone]"]').addClass("mask-fone");
    jQuery('input[name="billing[fax]"]').addClass("mask-fone");

    /* Máscaras nos campos de endereço de entrega (one page checkout) */
    jQuery('input[name="shipping[postcode]"]').mask("99999-999");
    jQuery('input[name="shipping[telephone]"]').addClass("mask-fone");
    jQuery('input[name="shipping[fax]"]').addClass("mask-fone");

    /* Máscaras nos campos da Conta do Usuário*/
    jQuery('input[name="postcode"]').mask("99999-999");
    jQuery('input[name="telephone"]').addClass("mask-fone");
    jQuery('input[name="fax"]').addClass("mask-fone");

    jQuery('.mask-fone').focus(function () {
        jQuery(this).mask("(99) 9999-9999?9");
    });
    jQuery('.mask-fone').focusout(function () {
        var phone, element;
        element = jQuery(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9"); }
    });

    /* Completa endereço em endereço de cobrança (one page checkout) */
    jQuery('input[name="billing[postcode]"]').keypress(function(ev){
        var content = this.value;
        content = content.replace('_','');
        content = content.replace('-','');        
        if(content.length > 7 && isNumber(ev)) {
            jQuery(this).postcodeAjax();
        }
    });

    /* Completa endereço em endereço de entrega (one page checkout) */
    jQuery('input[name="shipping[postcode]"]').keypress(function(ev){
        var content = this.value;
        content = content.replace('_','');
        content = content.replace('-','');        
        if(content.length > 7 && isNumber(ev)) {
            jQuery(this).postcodeAjax({
                postcode:       'input[name="shipping[postcode]"]',
                street:         'input[id="shipping:street1"]',
                number:         'input[id="shipping:street2"]',
                district:       'input[id="shipping:street4"]',
                city:           'input[name="shipping[city]"]',
                region:         'input[name="shipping[region_id]"]',
                rhidden:        'input[name="shipping[region]"]',
                regionSel:      'select[name="shipping[region_id]"]'
            });
        }
    });

    /* Completa endereço da Conta do Usuário */
    jQuery('input[name="postcode"]').keypress(function(ev){        
        var content = this.value;
        content = content.replace('_','');
        content = content.replace('-','');        
        if(content.length > 7 && isNumber(ev)) {
            jQuery(this).postcodeAjax({
                postcode:       'input[name="postcode"]',
                street:         'input[id="street_1"]',
                number:         'input[id="street_2"]',
                district:       'input[id="street_4"]',
                city:           'input[name="city"]',
                region:         'input[name="region_id"]',
                rhidden:        'input[name="region"]',
                regionSel:      'select[name="region_id"]'
            });
        }
    });
});


function isNumber(event){
  var charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57))
   return false;
 return true;
}
