<div class="footer-container">
    <div class="footer">
        <div class="footer-lv1">
            <div class="footer-col col1">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('institucional-footer')->toHtml() ?>
            </div>
            <div class="footer-col col2">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('produtos-footer')->toHtml() ?>
            </div>
            <div class="footer-col col3">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('carrinho-footer')->toHtml() ?>
            </div>
            <div class="footer-col col4">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('fb-footer')->toHtml() ?>
            </div>
        </div>
        <div class="footer-lv2">
            <div class="footer-col col1">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer-bandeiras')->toHtml() ?>
            </div>
            <div class="footer-col col2">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer-envio')->toHtml() ?>
            </div>
            <div class="footer-col col3">
                <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer-seguranca')->toHtml() ?>
            </div>
        </div>
        <address class="copyright"><?php echo $this->getCopyright() ?></address>
    </div>
</div>