<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-language-container f-left">
            <?php echo $this->getChildHtml('store_language') ?>
            <?php echo $this->getTelefone(); ?>
            <span class="chatonline">Chat Online</span>
        </div>
        <div class="f-right">
            <div class="f-left">
                <?php echo $this->getChildHtml('currency_switcher') ?>
            </div>
        </div>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="<?php echo $this->getUrl('') ?>">
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
        </a>
        <div class="right-header-container">
            <div class="right-header-top">
                <?php echo $this->getWelcome(); ?>
                <div id="header-links" class="skip-content">
                    <?php echo $this->getChildHtml('topLinks') ?>
                </div>
                <div class="language-switcher">
                    <?php echo $this->getChildHtml('store_language'); ?>
                </div>
            </div>
            <div class="right-header-bottom">
                <!-- Cart -->
                <div class="top-cart">
                    <?php echo $this->getChildHtml('minicart_head'); ?>
                    <span class="mini-cart-subtotal">
                        <?php
                        $cartSubtotal = Mage::getSingleton('checkout/session')->getQuote()->getSubtotal();
                        echo Mage::helper('checkout')->formatPrice($cartSubtotal);
                        ?>
                    </span>
                    <span class="mini-cart-itens">
                        <?php
                        $count = $this->helper('checkout/cart')->getSummaryCount();  //get total items in cart
                        if($count==0) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(0 itens)</a>',$count);
                        }
                        if($count==1) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(1 item)</a>',$count);
                        }
                        if($count>1) {
                            echo $this->__('<a href="/checkout/cart" class="cartgo">(%s itens)</a>',$count);
                        }
                        ?>
                    </span>
                </div>
                <!-- Search -->
                <div class="header-search-holder">
                    <div id="header-search" class="skip-content">
                        <?php echo $this->getChildHtml('topSearch') ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Skip Links -->
        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Menu'); ?></span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Search'); ?></span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Account'); ?></span>
            </a>

        </div>

        <!-- Account -->
        <div id="header-account" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
    </div>
    <!-- Navigation -->
    <div id="header-nav" class="skip-content">
        <div class="header-bar">
            <?php echo $this->getChildHtml('topMenu') ?>
        </div>
    </div>
    <?php echo $this->getAdditionalHtml() ?>
</header>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#select-store').each(function() {
            var select = jQuery(document.createElement('select')).insertBefore(jQuery(this));

            jQuery(".language-switcher select").append('<option value="" selected="selected">Selecione um idioma</option>');

            jQuery('>li a', this).each(function() {
                var target = jQuery(this).attr('target'),
                    option = jQuery(document.createElement('option'))
                        .appendTo(select)
                        .val(this.href)
                        .html(jQuery(this).html());
            });
        });

        jQuery(".language-switcher select").change(function() { window.location = jQuery(this).find("option:selected").val(); });

        jQuery("#select-store .current").click(function(){
            jQuery(this).parent().hide();
            jQuery(".language-switcher select").show();
        });

    });
</script>
<script type="text/javascript">
    (function($,sr){
        var debounce = function (func, threshold, execAsap) {
            var timeout;
            return function debounced () {
                var obj = this, args = arguments;
                function delayed () {
                    if (!execAsap)
                        func.apply(obj, args);
                    timeout = null;
                };
                if (timeout)
                    clearTimeout(timeout);
                else if (execAsap)
                    func.apply(obj, args);
                timeout = setTimeout(delayed, threshold || 100);
            };
        }
        jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
    })(jQuery,'smartresize');

    //Function to the css rule
    function checkSize(){
        var state = window.getComputedStyle(document.body,':before').content;
        if (state == '"responsive-top"') {
            jQuery("#header .skip-link.skip-cart").appendTo("#header .skip-links");
            jQuery("#header-cart").appendTo("#header .skip-links");
            jQuery("#header-search").appendTo("#header .skip-links");
        } else {
            jQuery("#header .skip-link.skip-cart").prependTo("#header .top-cart");
            jQuery("#header-cart").appendTo("#header .top-cart");
            jQuery("#header-search").appendTo("#header .header-search-holder");
        }
    }

    jQuery(document).ready(function(){
        checkSize();
    });

    jQuery(window).smartresize(function(){
        checkSize();
    });

</script>