<div class="footer-bar">
    <div class="footer-container">
        <div class="footer-news">
            <div class="textnews">
                <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_texto_news')->toHtml(); ?>
            </div>
            <?php echo $this->getLayout()->createBlock('newsletter/subscribe')->setTemplate('newsletter/subscribe.phtml')->toHtml(); ?>
        </div>
    </div>
</div>
<div class="footer-container">
    <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('custom_footer')->toHtml(); ?>
    <div class="full-col">
        <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_flags')->toHtml(); ?>
        <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('info-footer')->toHtml(); ?>

    </div>
</div>
<!-- Start of Async HubSpot Analytics Code -->
<script type="text/javascript">
    (function(d,s,i,r) {
        if (d.getElementById(i)){return;}
        var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
        n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/544196.js';
        e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
</script>
<!-- End of Async HubSpot Analytics Code -->