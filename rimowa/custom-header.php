<?php
$_cartQty = $this->getSummaryCount()? $this->getSummaryCount() : 0;
?>
    <div class="header-language-background">
        <div class="header-language-container">
            <div class="f-left top-line-1" style="color:#383838"><strong>SAC 0800 746 6920</strong> - Segunda a Sexta das 10:00h às 18:00h | Sábados das 10:00h às 17:00h</div>
            <div class="f-right top-line-2">
                <?php echo $this->getChildHtml('currency_switcher') ?>
                <?php echo $this->getChildHtml('topLinks') ?>

                <div class="header-minicart">
                    <?php echo $this->getChildHtml('minicart_head'); ?>
                </div>
            </div>
        </div>
    </div>

    <header id="header" class="page-header">
        <div class="page-header-container">

            <a class="logo" href="<?php echo $this->getUrl('') ?>">
                <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
                <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
            </a>

            <!-- Skip Links -->
            <div class="skip-links">
                <a href="#header-nav" class="skip-link skip-nav">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Menu'); ?></span>
                </a>

                <a href="#header-search" class="skip-link skip-search">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Search'); ?></span>
                </a>

                <a href="#header-account" class="skip-link skip-account">
                    <span class="icon"></span>
                    <span class="label"><?php echo $this->__('Account'); ?></span>
                </a>
            </div>

            <!-- Navigation -->
            <div id="header-nav" class="skip-content">
                <?php echo $this->getChildHtml('topMenu') ?>
            </div>

            <!-- Search -->
            <div id="header-search" class="skip-content">
                <?php echo $this->getChildHtml('topSearch') ?>
            </div>

            <?php // In order for the language switcher to display next to logo on smaller viewports, it will be moved here.
            // See app.js for details ?>

            <?php echo $this->getWelcome(); ?>

            <!-- Account -->
            <div id="header-account" class="skip-content">
                <?php echo $this->getChildHtml('topLinks') ?>
            </div>
        </div>

        <?php echo $this->getAdditionalHtml() ?>
    </header>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>
<script type="text/javascript">
    (function($,sr){
        var debounce = function (func, threshold, execAsap) {
            var timeout;
            return function debounced () {
                var obj = this, args = arguments;
                function delayed () {
                    if (!execAsap)
                        func.apply(obj, args);
                    timeout = null;
                };
                if (timeout)
                    clearTimeout(timeout);
                else if (execAsap)
                    func.apply(obj, args);
                timeout = setTimeout(delayed, threshold || 100);
            };
        }
        jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
    })(jQuery,'smartresize');

    //Function to the css rule
    function checkSize(){
        var state = window.getComputedStyle(document.body,':before').content;
        if (state == '"responsive-top"') {
            jQuery(".top-line-2 .header-minicart .skip-cart").appendTo(".page-header-container .skip-links");
            jQuery(".top-line-2 .header-minicart #header-cart").appendTo(".page-header-container .skip-links");
        } else {
            jQuery(".page-header-container .skip-links .skip-cart").prependTo(".top-line-2 .header-minicart");
            jQuery(".page-header-container .skip-links #header-cart").prependTo(".top-line-2 .header-minicart");
        }
    }

    jQuery(document).ready(function(){
        checkSize();
    });

    jQuery(window).smartresize(function(){
        checkSize();
    });

</script>