<?php $_helper = $this->helper('catalog/output'); ?>
<?php $_product = $this->getProduct(); ?>

<?php
/*=================================
OutOfStock Subctiption: Início
=================================*/

$_storeId = Mage::app()->getStore()->getId();
$_isActive = Mage::getStoreConfig('outofstocksubscription/mail/active', $_storeId);
$qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
if(!$_product->isSaleable() && $_isActive):
    $_url = $this->getUrl('outofstocksubscription');
else:
    $_url = $this->getSubmitUrl($_product);
endif;

/*=================================
OutOfStock Subctiption: Fim
=================================*/
?>

<script type="text/javascript">
    var optionsPrice = new Product.OptionsPrice(<?php echo $this->getJsonConfig() ?>);
</script>
<div id="messages_product_view"><?php echo $this->getMessagesBlock()->getGroupedHtml() ?></div>
<div class="product-view">
    <div class="product-essential">
        <form action="<?php echo $_url; ?>" method="post" id="product_addtocart_form"<?php if($_product->getOptions()): ?> enctype="multipart/form-data"<?php endif; ?>>
            <?php echo $this->getBlockHtml('formkey') ?>
            <div class="no-display">
                <input type="hidden" name="product" value="<?php echo $_product->getId() ?>" />
                <input type="hidden" name="related_product" id="related-products-field" value="" />
            </div>
            <div class="container-product-basic-and-image-gallery">

                <div class="product-img-box">
                    <div class="product-name">
                        <h1><?php echo $_helper->productAttribute($_product, $_product->getName(), 'name') ?></h1>
                    </div>
                    <?php echo $this->getChildHtml('media') ?>
                </div>

                <div class="product-shop">
                    <div class="product-name">
                        <h1><?php echo $_helper->productAttribute($_product, $_product->getName(), 'name') ?></h1>
                    </div>

                    <div class="extra-info">
                        <?php echo $this->getReviewsSummaryHtml($_product, 'default', false)?>
                    </div>

                    <?php echo $this->getChildHtml('alert_urls') ?>

                    <?php if ($_product->getShortDescription()):?>
                        <div class="short-description">
                            <div class="std"><?php echo $_helper->productAttribute($_product, ($_product->getShortDescription()), 'short_description') ?></div>
                        </div>
                    <?php endif;?>


                    <?php echo $this->getChildHtml('other');?>

                    <?php if ($_product->isSaleable() && $this->hasOptions()):?>
                        <?php echo $this->getChildChildHtml('container1', '', true, true) ?>
                    <?php endif;?>

                    <div class="container-sharing">
                        <span class="label-sharing"><?php echo $this->__("Compartilhe:");?></span>
                        <a href="javascript:popWin('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($productUrl); ?>&t=<?php echo urlencode($productName); ?>', 'facebook', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="<?php echo $this->__('Compartilhe no Facebook') ?>" id="btn-facebook" class="social-btn d-icon-facebook">Rimowa Facebook</a>
                        <a href="javascript:popWin('https://plus.google.com/share?url=<?php echo urlencode($productUrl); ?>', 'google', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="<?php echo $this->__('Compartilhe no Google Plus') ?>" class="social-btn d-icon-gplus">Rimowa Google Plus</a>
                    </div>
                </div>
            </div>
            <div class="add-to-cart-wrapper-container">
                <div class="add-to-cart-wrapper">

                    <?php echo $this->getChildHtml('product_type_availability'); ?>
                    <?php if(!$_product->isSaleable() && $_isActive): ?>
                        <?php echo $this->getChildHtml('out_of_stock') ?>
                    <?php else: ?>
                        <div class="price-info">
                            <?php echo $this->getPriceHtml($_product); ?>
                            <?php echo $this->getChildHtml('bundle_prices') ?>
                            <?php echo $this->getChildHtml('boleto_parcelas') ?>
                            <?php echo $this->getTierPriceHtml() ?>
                            <?php echo $this->getChildHtml('extrahint') ?>
                        </div>

                        <?php echo $this->getChildHtml('product_type_data') ?>
                        <?php if (!$this->hasOptions()):?>

                            <div class="available-qty-container">
                                <span class="available-qty-label"><?php echo $this->__('Quantidade disponível:'); ;?></span>
                                <span class="available-qty"><?php echo $qtyStock;?></span>
                            </div>

                            <div class="add-to-box">
                                <?php if($_product->isSaleable()): ?>
                                    <?php echo $this->getChildHtml('addtocart') ?>
                                    <?php if( $this->helper('wishlist')->isAllow() || $_compareUrl=$this->helper('catalog/product_compare')->getAddUrl($_product)): ?>
                                        <span class="or"><?php echo $this->__('OR') ?></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php echo $this->getChildHtml('addto') ?>
                            </div>
                            <?php echo $this->getChildHtml('extra_buttons') ?>
                        <?php elseif (!$_product->isSaleable()): ?>
                            <div class="available-qty-container">
                                <span class="available-qty-label"><?php echo $this->__('Quantidade disponível:'); ;?></span>
                                <span class="available-qty"><?php echo $qtyStock;?></span>
                            </div>
                            <div class="add-to-box">
                                <?php echo $this->getChildHtml('addto') ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($_product->isSaleable() && $this->hasOptions()):?>
                            <?php echo $this->getChildChildHtml('container2', '', true, true) ?>
                        <?php endif;?>
                    <?php endif; ?>
                </div>

                <?php if($_product->isSaleable()): ?>
                    <?php echo $this->getChildHtml('frete') ?>
                <?php endif; ?>
            </div>

            <div class="clearer"></div>
        </form>
        <script type="text/javascript">
            //<![CDATA[
            var productAddToCartForm = new VarienForm('product_addtocart_form');
            productAddToCartForm.submit = function(button, url) {
                if (this.validator.validate()) {
                    var form = this.form;
                    var oldUrl = form.action;

                    if (url) {
                        form.action = url;
                    }
                    var e = null;
                    try {
                        this.form.submit();
                    } catch (e) {
                    }
                    this.form.action = oldUrl;
                    if (e) {
                        throw e;
                    }

                    if (button && button != 'undefined') {
                        button.disabled = true;
                    }
                }
            }.bind(productAddToCartForm);

            productAddToCartForm.submitLight = function(button, url){
                if(this.validator) {
                    var nv = Validation.methods;
                    delete Validation.methods['required-entry'];
                    delete Validation.methods['validate-one-required'];
                    delete Validation.methods['validate-one-required-by-name'];
                    // Remove custom datetime validators
                    for (var methodName in Validation.methods) {
                        if (methodName.match(/^validate-datetime-.*/i)) {
                            delete Validation.methods[methodName];
                        }
                    }

                    if (this.validator.validate()) {
                        if (url) {
                            this.form.action = url;
                        }
                        this.form.submit();
                    }
                    Object.extend(Validation.methods, nv);
                }
            }.bind(productAddToCartForm);
            //]]>
        </script>
    </div>

    <div class="product-collateral toggle-content tabs">
        <?php if ($detailedInfoGroup = $this->getChildGroup('detailed_info', 'getChildHtml')):?>
            <dl id="collateral-tabs" class="collateral-tabs">
                <?php foreach ($detailedInfoGroup as $alias => $html):?>
                    <dt class="tab"><span><?php echo $this->escapeHtml($this->getChildData($alias, 'title')) ?></span></dt>
                    <dd class="tab-container">
                        <div class="tab-content"><?php echo $html ?></div>
                    </dd>
                <?php endforeach;?>
            </dl>
        <?php endif; ?>
    </div>
    <?php echo $this->getChildHtml('related_products') ?>

    <?php echo $this->getChildHtml('upsell_products') ?>
    <?php echo $this->getChildHtml('product_additional_data') ?>

</div>