<div class="footer-container">
    <div class="footer-wrapper">
        <div class="footer">
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('sobre-a-rimowa')->toHtml(); ?>
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links')->toHtml(); ?>
        </div>
    </div>
    <div class="footer">
        <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_icons')->toHtml(); ?>
    </div>
    <address class="copyright">
        <div class="footer">
            Copyright © 2001-<?php echo date('Y'); ?> www.rimowashop.com.br, TODOS OS DIREITOS RESERVADOS. As fotos aqui veiculadas, logotipo e marca são de propriedade do site www.rimowashop.com.br. É vetada a reprodução total ou parcial do conteúdo deste site sem a expressa autorização da administradora Rimowa América do Sul Malas de Viagem LTDA, CNPJ 08.733.713/0007-61. Imagens meramente ilustrativas.
        </div>
    </address>
</div>