<div class="header-language-background">
    <div class="header-language-container">
        <div class="header-container-left">
            <?php echo $this->getWelcome(); ?>
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
        <div class="header-container-right">
            <?php echo $this->getTelefone(); ?>
        </div>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="<?php echo $this->getUrl('') ?>">
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
        </a>

        <!-- Skip Links -->
        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Menu'); ?></span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Search'); ?></span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Account'); ?></span>
            </a>

            <!-- Cart -->
            <?php echo $this->getChildHtml('minicart_head'); ?>
            <a href="<?php echo Mage::getUrl('checkout/cart');?>" class="mini-cart-subtotal">
                <?php
                $cartSubtotal = Mage::getSingleton('checkout/session')->getQuote()->getSubtotal();
                echo Mage::helper('checkout')->formatPrice($cartSubtotal);
                ?>
            </a>
        </div>

        <!-- Search -->
        <div id="header-search" class="skip-content">
            <?php echo $this->getChildHtml('topSearch') ?>
        </div>

        <!-- Account -->
        <div id="header-account" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
    </div>
    <?php echo $this->getAdditionalHtml() ?>
</header>
<!-- Navigation -->
<div id="header-nav" class="skip-content">
    <div class="header-nav-container">
        <?php echo $this->getChildHtml('topMenu') ?>
    </div>
</div>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>