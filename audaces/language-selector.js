jQuery(document).ready(function(){
    jQuery('#select-store').each(function() {
        var select = jQuery(document.createElement('select')).insertBefore(jQuery(this));

        jQuery(".language-switcher select").append('<option value="" selected="selected">Selecione um idioma</option>');

        jQuery('>li a', this).each(function() {
            var target = jQuery(this).attr('target'),
                option = jQuery(document.createElement('option'))
                    .appendTo(select)
                    .val(this.href)
                    .html(jQuery(this).html());
        });
    });

    jQuery(".language-switcher select").change(function() { window.location = jQuery(this).find("option:selected").val(); });

    jQuery("#select-store .current").click(function(){
        jQuery(this).parent().hide();
        jQuery(".language-switcher select").show();
    });

});