<?php $_cartQty = $this->getSummaryCount()? $this->getSummaryCount() : 0; ?>
<?php
$_cartQty = $this->getSummaryCount();
if(empty($_cartQty)) {
    $_cartQty = 0;
}
?>
<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-tel-container f-left">
            <?php echo $this->getTelefone(); ?>
        </div>
        <div class="f-right">
            <div class="redes-sociais f-right">
                <a class="icon-twitter" href="https://twitter.com/sofaststore" tittle="Siga nos no Twitter!" class="twitter">Twitter - So Fast</a>
                <a class="icon-instagram" href="http://instagram.com/sofast.com.br/" title="Siga nos no Instagram!" class="instagram">Instagram - So Fast</a>
                <a class="icon-youtube" href="https://www.youtube.com/user/sofaststore" class="youtube" title="Youtube - So Fast">Youtube So Fast</a>
                <a class="icon-face" href="https://www.facebook.com/sofaststore" title="Curta nossa página no Facebook!" class="facebook">Facebook - So Fast</a>
            </div>
            <div class="top-links-container f-right">
                <?php echo $this->getChildHtml('topLinks') ?>
            </div>
        </div>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="<?php echo $this->getUrl('') ?>">
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
        </a>

        <?php // In order for the language switcher to display next to logo on smaller viewports, it will be moved here.
        // See app.js for details ?>
        <div class="store-language-container"></div>

        <?php echo $this->getWelcome(); ?>

        <!-- Skip Links -->
        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Menu'); ?></span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Search'); ?></span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Account'); ?></span>
            </a>

            <div id="header-links">
                <span class="my-account-link"><a href="/customer/account">Minha Conta</a></span>
                <span class="my-orders-link"><a href="/sales/order/history/">Meus Pedidos</a></span>
            </div>

            <!-- Cart -->
            <?php echo $this->getChildHtml('minicart_head');?>
        </div>

        <!-- Search -->
        <div id="header-search" class="skip-content">
            <?php echo $this->getChildHtml('topSearch') ?>
        </div>

        <!-- Account -->
        <div id="header-account" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
    </div>
    <!-- Navigation -->
    <div id="header-nav" class="skip-content">
        <?php echo $this->getChildHtml('topMenu') ?>
    </div>
    <?php echo $this->getAdditionalHtml() ?>
</header>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>
<div class="page-header">
    <?php echo $this->getChildHtml('banner_secundario'); ?>
</div>