<div class="footer-bar">
    <div class="footer-container">
        <div class="textnews">
            <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_texto_news')->toHtml(); ?>
        </div>
        <div class="footer-news">
            <?php echo $this->getLayout()->createBlock('newsletter/subscribe')->setTemplate('newsletter/subscribe.phtml')->toHtml(); ?>
        </div>
    </div>
</div>
<div class="footer-container">
    <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('custom_footer')->toHtml(); ?>
    <div class="sub-cols sub-col5"><? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_like_box')->toHtml(); ?></div>
    <div class="full-col">
        <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_flags')->toHtml(); ?>
    </div>
    <? echo $this->getLayout()->createBlock('cms/block')->setBlockId('info-footer')->toHtml(); ?>
</div>



<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?1XeRXAWIM3DW7WgnvhzjlLDlXWPNOrbW';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->