<div class="footer-newsletter">	
	<div class="footer-newsletter-container">	
		<div class="textnews">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_texto_news')->toHtml(); ?>
		</div>
		<div class="footer-news">
			<?php echo $this->getLayout()->createBlock('newsletter/subscribe')->setTemplate('newsletter/subscribe.phtml')->toHtml(); ?>
		</div>
	</div>
</div>
<div class="footer-container">
    <div class="footer">		
        <div class="footer-top">		
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('custom_footer')->toHtml() ?>				
		</div>			        
    </div>
</div>
<div class="footer-bottom">
	<div class="footer-bottom-container">
		<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_icons')->toHtml() ?>		
	</div>	
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
  jQuery("#newsletter").attr("placeholder","Digite aqui o seu email...");
})
</script>	
