<div id="topo-topo">
</div>

<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-language-container f-left">
            <?php echo $this->getChildHtml('store_language') ?>
            <?php echo $this->getTelefone(); ?>
            <span class="chatonline">Chat Online</span>
        </div>
        <div class="f-right">
            <div class="f-left">
                <?php echo $this->getChildHtml('currency_switcher') ?>
            </div>
        </div>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="<?php echo $this->getUrl('') ?>">
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
        </a>

        <?php // In order for the language switcher to display next to logo on smaller viewports, it will be moved here.
        // See app.js for details ?>
        <div class="store-language-container"></div>

        <?php echo $this->getWelcome(); ?>


        <!-- Skip Links -->
        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Menu'); ?></span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Search'); ?></span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Account'); ?></span>
            </a>

            <!-- Cart -->
            <div class="header-minicart">
                <?php echo $this->getChildHtml('minicart_head'); ?>
            </div>
        </div>

        <div id="header-links" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>

        <!-- Search -->
        <div id="header-search" class="skip-content mini-search">
            <?php echo $this->getChildHtml('topSearch') ?>
        </div>

        <!-- Account -->
        <div id="header-account" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
    </div>
    <!-- Navigation -->
    <div id="header-nav" class="skip-content">
        <div class="header-bar">
            <!-- Search -->
            <div id="header-search" class="skip-content">
                <?php echo $this->getChildHtml('topSearch') ?>
            </div>
            <?php echo $this->getChildHtml('topMenu') ?>
        </div>
    </div>
    <?php echo $this->getAdditionalHtml() ?>
</header>
<div class="header-middle">
    <div class="header-middle-container">
        <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('acima_banner')->toHtml(); ?>
        <?php echo $this->getChildHtml('topContainer'); ?>
        <?php echo $this->getChildHtml('topBanner'); ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>Social</title>
            <link rel="stylesheet" href="redesocial.css">
            <link rel="stylesheet" href="fonts.css">
        </head>
        <body>
        <div class="social">
            <ul>
                <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook"></a></li>
                <li><a href="http://www.twitter.com" target="_blank" class="icon-twitter"></a></li>
                <li><a href="http://www.googleplus.com" target="_blank" class="icon-google-plus"></a></li>
                <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                <li><a href="mailto:contato@jackreeves.com.br" class="icon-mail4"></a></li>
            </ul>
        </div>
        </body>
        </html>
    </div>
</div>