<div class="container_12 top-container">
	<div class="line1 clear-2">
		<div class="grid_6">
			<i class="fa fa-phone"></i> Telefones: <?php echo $this->getTelefone(); ?>
			Segunda à Sexta : 9h às 18:45h
			Sábado: 9h às 17h
		</div>
		<div class="grid_6">
			<a href="<?php echo $this->getUrl('lista-de-casamento-supreme-inox-tramontina');?>" class="btn-wedding" title="Lista de Casamento">
				<img src="/media/images/default/lista-de-casamento-topo.jpg" alt="Lista de Casamento" />
			</a>
			<!-- Link para logout -->
			<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>
			<span class="logout-link f-right">
				[<a href="<?php echo $this->getUrl('customer/account/logout/'); ?>" title="Encerrar sessão">sair</a>]
			</span>
			<?php endif; ?>

			<!-- Mensagem de boas-vindas -->
		    <?php echo $this->getWelcome() ?>
		</div>
                    <div class="clear"></div>
                </div>

            <div class="grid_12">
                <?php if ($this->getIsHomePage()):?>
					<h1 class="logo">
						<a href="<?php echo $this->getUrl('') ?>" title="<?php echo $this->getLogoAlt(); ?>">
							<img src="<?php echo $this->getLogoSrc(); ?>" alt="<?php echo $this->getLogoAlt(); ?>" />
						</a>
					</h1>
				<?php else:?>
					<a href="<?php echo $this->getUrl('') ?>" title="<?php echo $this->getLogoAlt(); ?>" class="logo">
						<img src="<?php echo $this->getLogoSrc(); ?>" alt="<?php echo $this->getLogoAlt(); ?>" />
					</a>
				<?php endif?>


                <?php echo $this->getChildHtml('topSearch') ?>

				<div class="atendimento-topo">
					<script type="text/javascript" language="JavaScript" src="https://www.chatcomercial.com.br/livehelp/lib/javascript/support_status.php?COMPANY_ID=18657&amp;SITE_ID=22030&amp;ssl=1"></script>
				</div>

				<ul class="links">
                	<li class="top-wishlist"><a href="<?php echo $this->getUrl('wishlist'); ?>" title="Meus Favoritos" class="wishlist-link">Meus Favoritos</a></li>
					<li class="top-orders"><a href="<?php echo $this->getUrl('sales/order/history/'); ?>" title="Meus Pedidos" class="top-link-account">Meus Pedidos</a></li>
				</ul>
                <?php echo $this->getChildHtml('topCart') ?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="header-wrapper">
            <div class="container_12">
                <div class="grid_12">
                    <?php echo $this->getChildHtml('topMenu') ?>
                </div>
                <div class="clear"></div>
            </div>
			<div class="top-block-wrapper">
            	<?php echo $this->getChildHtml('top_block') ?>
			</div>
        </div>
        <div class="container_12">
            <div class="grid_12">
                <?php echo $this->getChildHtml('header_slider_container') ?>
            </div>
        </div>
		<?php echo $this->getChildHtml('topContainer'); ?>
		<?php echo $this->getChildHtml('top_block_2') ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
  if(jQuery("body").hasClass("customer-account-create")){
    if(!jQuery("#is_subscribed").is(":checked")){
      jQuery("#is_subscribed").prop('checked', true);
    }
  }
});
</script>