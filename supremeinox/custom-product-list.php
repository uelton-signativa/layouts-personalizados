<?php echo $this->getToolbarHtml(); ?>
<ul class="products-grid">
    <?php $i=0; foreach ($_productCollection as $_product): ?>
        <?php $_productNameStripped = $this->stripTags($_product->getName(), null, true); ?>
        <li class="item">
            <div class="product-img-box">
                <a href="<?php echo $_product->getProductUrl() ?>" title="<?php echo $this->stripTags($this->getImageLabel($_product, 'small_image'), null, true) ?>" class="product-image">
                    <img src="<?php echo $this->helper('catalog/image')->init($_product, 'small_image')->resize(272); ?>" alt="<?php echo $this->stripTags($this->getImageLabel($_product, 'small_image'), null, true) ?>" />
                </a>
            </div>

            <a class="product-name" href="<?php echo $_product->getProductUrl() ?>" title="<?php echo $this->stripTags($_product->getName(), null, true) ?>"><?php echo $_helper->productAttribute($_product, $_product->getName(), 'name') ?></a>
            <?php
            //executa o product labels se o mesmo estiver ativo na loja
            if(Mage::getConfig()->getModuleConfig('EM_Productlabels')->is('active', 'true')) {
                Mage::helper('productlabels')->display($_product,'image_on_list');
            }
            ?>

            <?php echo $this->getPriceHtml($_product, true) ?>
            <div class="nr-pcl-container">
                <?php echo Mage::helper('parcelamento')->showMaxParcelas($_product,'small'); ?>
            </div>
            <div class="clear"></div>
            <a href="<?php echo $_product->getProductUrl() ?>" class="hover-box">
                <button type="button" title="<?php echo $this->__('Add to Cart') ?>" class="button btn-cart" onclick="setLocation('<?php echo $this->getAddToCartUrl($_product) ?>')"><span><span><?php echo $this->__('Add to Cart') ?></span></span></button>
            </a>
        </li>
    <?php endforeach ?>
</ul>
<script type="text/javascript">decorateGeneric($$('ul.products-grid li'), ['odd','even','first','last'])</script>
<div class="toolbar-bottom">
    <?php echo $this->getToolbarHtml(); ?>
</div>