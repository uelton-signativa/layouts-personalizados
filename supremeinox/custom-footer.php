<?php echo $this->getChildHtml('footer_subscribe'); ?>
<div class="container_12 footer_banner">
<?php echo Mage::app()->getLayout()->createBlock('banner/banner')->setName("footer_banners")->setBannerGroupCode("footer_banners")->setTemplate('unibanner/banner.phtml')->toHtml(); ?>
</div>
<div class="container_12">
	<?php echo $this->getChildHtml('footer_icons') ?>
</div>
<div class="footer-container">
	<div class="footer-content container_12">
		<div class="grid_2 col1">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links1')->toHtml(); ?>
		</div>
		<div class="grid_2 col2">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links2')->toHtml(); ?>
		</div>
		<div class="grid_2 col3">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links3')->toHtml(); ?>
		</div>
		<div class="grid_2 col4">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links4')->toHtml(); ?>
		</div>
		<div class="grid_4 col5">
			<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer_links5')->toHtml(); ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="container_12 a-center">
	<p>Preços, prazos de pagamento e promoções exclusivos para compras realizadas através do site. Não se aplicam as nossas lojas físicas.</p>
	<p>Supreme Inox Comercial LTDA  |  CNPJ 07.487.938/0001-27  | Av. Cristóvão Colombo, 1230 - Bairro Floresta - Porto Alegre/RS</p>
</div>