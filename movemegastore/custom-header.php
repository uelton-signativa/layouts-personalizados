<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-language-container f-left">
            <?php echo $this->getChildHtml('store_language') ?>
            <?php echo $this->getTelefone(); ?>

        </div>
        <div class="f-right">
            <div class="f-left">
                <?php echo $this->getChildHtml('currency_switcher') ?>
                <?php echo $this->getChildHtml('topLinks') ?>
            </div>

        </div>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="<?php echo $this->getUrl('') ?>">
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="large" />
            <img src="<?php echo $this->getLogoSrc() ?>" alt="<?php echo $this->getLogoAlt() ?>" class="small" />
        </a>

        <?php // In order for the language switcher to display next to logo on smaller viewports, it will be moved here.
        // See app.js for details ?>
        <div class="store-language-container"></div>

        <?php echo $this->getWelcome(); ?>

        <!-- Search -->
        <div id="header-search" class="skip-content">
            <?php echo $this->getChildHtml('topSearch') ?>
        </div>

        <!-- Skip Links -->
        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Menu'); ?></span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Search'); ?></span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label"><?php echo $this->__('Account'); ?></span>
            </a>

            <!-- Cart -->
            <div class="header-minicart">
                <?php echo $this->getChildHtml('minicart_head'); ?>
            </div>
        </div>

        <div id="header-links" class="skip-content">
            <span class="favorito"><a href="/wishlist">Favoritos</a></span>
            <span class="meus-pedidos"><a href="/sales/order/history/">Meus Pedidos</a></span>
        </div>

        <!-- Account -->
        <div id="header-account" class="skip-content">
            <?php echo $this->getChildHtml('topLinks') ?>
        </div>
    </div>
    <!-- Navigation -->
    <div id="header-nav" class="skip-content">
        <?php echo $this->getChildHtml('topMenu') ?>
    </div>
    <?php echo $this->getAdditionalHtml() ?>
</header>
<?php echo $this->getChildHtml('topContainer'); ?>
<?php echo $this->getChildHtml('topBanner'); ?>